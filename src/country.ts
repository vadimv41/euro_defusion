import {Motif} from "./interfaces/motif.interface";
import {City} from "./city";
import {DEFAULT_BALANCE} from "./constants";

export class Country {
  private readonly _name: string;
  private readonly countryCities: City[] = [];
  public days: number;
  public readonly xl: number;
  public readonly yl: number;
  public readonly xh: number;
  public readonly yh: number;
  public completed: boolean;

  get cities() {
    return this.countryCities;
  }

  get name() {
    return this._name;
  }

  constructor(name: string, xl: number, yl: number, xh: number, yh: number) {
    if (name.length <= 25) {
      this._name = name;
    } else {
      throw new Error('Name length must be less then 25 characters');
    }
    if ((1 <= xl && xl <= xh && xh <= 10) && (1 <= yl && yl <= yh && yh <= 10)) {
      this.countryCities = this.createCountryMap(xl, yl, xh, yh);
      this.xl = xl;
      this.yl = yl;
      this.xh = xh;
      this.yh = yh;
      this.completed = false;
      this.days = 0;
    } else {
      throw new Error(`Incorrect values xl, yl, xh, yh for country ${name}`);
    }
  }


  private createCountryMap(xl, yl, xh, yh) : City[] {
    const countryMap: City[] = [];
    for (let x = xl; x <= xh; x++) {
      for (let y = yl; y <= yh; y++) {
        countryMap.push(
          new City(
          x,
          y,
          [
            {
              currency: this.name,
              value: DEFAULT_BALANCE,
            } as Motif
          ],
            this.name));
      }
    }


    return countryMap;
  }
}
