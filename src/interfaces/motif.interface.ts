export interface Motif {
  currency: string;
  value: number;
}
