import {Motif} from "./motif.interface";

export interface CityInterface {
  x: number;
  y: number;
  motifs: Motif[];
}
