import {Country} from './country';
import {City} from './city';
import {uniqBy} from 'lodash';
import {MOTIF_DIVISION} from "./constants";

export class WorldMap {
  public worldMapCountries: Country[];

  constructor(countries: Country[]) {
    if (this.validateCountries(countries)) {
      this.worldMapCountries = countries;
    }
  }

  private validateCountries(countries: Country[]) {
    let flag = false;
    if (countries.length > 1) {
      for (let i = 0; i < countries.length; i++) {
        countries[i].cities.forEach(city => {
          for (let j = i + 1; j < countries.length; j++) {
            countries[j].cities.forEach(neighborCity => {
              if ((city.x === neighborCity.x && (neighborCity.y === city.y + 1 || neighborCity.y === city.y - 1)) ||
                (city.y === neighborCity.y && (neighborCity.x === city.x + 1 || neighborCity.x === city.x - 1))) {
                flag = true;
              }
            });
          }
        })
      }
      if (!flag) {
        throw new Error('Countries must have neighbors');
      }
    }
    const uniqueCountries = uniqBy(countries, 'name');
    if (countries.length !== uniqueCountries.length) {
      throw new Error('Countries names must be unique');
    }
    return true;
  }

  get countries() {
    return this.worldMapCountries;
  }

  public calculateEuroDiffusion() {
    let day = 0;

    while (1) {
      let motifsToSend = [];
      const worldMap = [];
      this.worldMapCountries.forEach(el => {
        worldMap.push(...el.cities);
      });
      this.worldMapCountries.forEach(country => {
        country.cities.forEach(city =>{
          this.checkCityComplete(city);
          this.setCompleteDaysToCountry(country, day);
        });
      });

      if (this.checkIfAllCountriesCompleted()) return;


      worldMap.forEach(city => {
        const cityMotifsToSend = this.calculateCityMotifsToSend(day, city);
        if (cityMotifsToSend) {
          motifsToSend.push(cityMotifsToSend);
        }
      });

      motifsToSend.forEach(motifsData => {
        if (motifsData.motifsToSend.length > 0) {
          const rightCity = worldMap.find(el => el.x === motifsData.city.x + 1 && el.y === motifsData.city.y);
          const leftCity = worldMap.find(el => el.x === motifsData.city.x - 1 && el.y === motifsData.city.y);
          const upperCity = worldMap.find(el => el.x === motifsData.city.x && el.y === motifsData.city.y + 1);
          const lowerCity = worldMap.find(el => el.x === motifsData.city.x && el.y === motifsData.city.y - 1);
          motifsData.motifsToSend.forEach(el => {
            this.transfer(rightCity, motifsData.city, el);
            this.transfer(leftCity, motifsData.city, el);
            this.transfer(upperCity, motifsData.city, el);
            this.transfer(lowerCity, motifsData.city, el);
          })
        }
      });

      day++;
    }
  }

  checkCityComplete(city) {
    if(Object.keys(city.balance).length === this.worldMapCountries.length && !city.completed){
      city.completed = true;
    }
  }

  checkIfAllCountriesCompleted () {
    let allCompleted = true;
    this.worldMapCountries.forEach(country => {
      if (!country.completed) {
        allCompleted = false;
        return allCompleted;
      }
    });
    return allCompleted;
  }

  setCompleteDaysToCountry(country, day) {
    let countryCompleted = true;
    if (!country.completed) {
      country.cities.forEach(city => {
        if (!city.completed) {
          countryCompleted = false;
        }
      });
      if (countryCompleted) {
        country.days = day;
        country.completed = true;
      }
    }
  }

  transfer(receiver, sender, portion) {
    if (receiver) {
      const countryNames = Object.keys(portion);
      sender.transfer(countryNames[0], -portion[countryNames[0]]);
      receiver.transfer(countryNames[0], portion[countryNames[0]]);
    }
  }

  private calculateCityMotifsToSend(day, city: City) {
    let motifsToSend = [];
    Object.keys(city.balance).forEach(key => {
      const motif = Math.floor(city.balance[key] / MOTIF_DIVISION);
      if( motif !== 0 ){
        motifsToSend.push({[key]: motif});
      }
    });
    return { city, motifsToSend };
  }

  public printResult() {
    let countries = this.worldMapCountries
      .sort((a: Country, b: Country) => {
        if (a.name > b.name) {
          return 1;
        } else {
          return -1;
        }
      })
      .sort((a: Country, b: Country) => {
      if (a.days > b.days) {
        return 1;
      } else {
        return -1;
      }
    });
    countries.forEach(el => {
      console.log(`${el.name} ${el.days}`)
    });

  }

}
