import {WorldMap} from "./world.map";
import {Reader} from "./reader";
const reader = new Reader();

try {
  const worldsArr = reader.prepareDataFromFile();
  let i = 1;

  worldsArr.forEach(worldArr => {
    let w = new WorldMap(worldArr);
    w.calculateEuroDiffusion();
    console.log(`Case Number ${i}`);
    w.printResult();
    i++;
  });

} catch (e) {
  console.error(`Error: ${e.message}`);
}
