import * as fs from "fs";
import {Country} from "./country";

export class Reader {

  prepareDataFromFile() {
    const fileData = fs.readFileSync('data.txt').toString();
    let lines = fileData.split('\n').filter(el => el !== '');
    const worldsArr = [];

    if (isNaN(Number(lines[0]))) {
      throw new Error('First line must be a number');
    }
    let world = [];
    let countriesCount = 0;
    for (const line of lines) {
      if (!isNaN(Number(line)) ) {
        if (world.length > 0) {
          if (world.length === countriesCount) {
            worldsArr.push(world);
            world = [];
          } else {
            throw new Error('Countries count is incorrect');
          }
        }
        countriesCount = Number(line);
      } else {
        const countryData = line.split(' ');
        if (countryData.length < 5) {
          throw new Error('Missing country params');
        }
        if (countryData[0].length > 25) {
          throw new Error('Country name must be shorter than 25 characters');
        }
        if (isNaN(Number(countryData[1])) || isNaN(Number(countryData[2])) || isNaN(Number(countryData[3])) || isNaN(Number(countryData[4]))) {
          throw new Error('Coordinates must be a numbers');
        }
        world.push(new Country(countryData[0], Number(countryData[1]), Number(countryData[2]), Number(countryData[3]), Number(countryData[4])));
      }

      if (line === '0') {
        break;
      }
    }
    return worldsArr;
  }
}
