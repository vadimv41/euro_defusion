import {Motif} from "./interfaces/motif.interface";
import {DEFAULT_BALANCE} from "./constants";

export class City {
  private readonly xCoord: number;
  private readonly yCoord: number;
  private motifsArray: Motif[];
  private country: string;
  public completed: boolean;
  public balance = {};

  constructor(x: number, y: number, motifs: Motif[], country) {
    this.xCoord = x;
    this.yCoord = y;
    this.motifsArray = motifs;
    this.balance[country] = DEFAULT_BALANCE;
    this.country = country;
    this.completed = false;
  }

  get x() {
    return this.xCoord;
  }

  get y() {
    return this.yCoord;
  }

  transfer(country, amount) {
    if (this.balance[country]) {
      this.balance[country] += Number(amount)
    } else {
      this.balance[country] = Number(amount);
    }
  }
}
